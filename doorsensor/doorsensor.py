# -*- coding: utf-8 -*-
from gpiozero import Button, LED, TonalBuzzer
from gpiozero.tools import sin_values
from time import sleep
from gpiozero.tones import Tone
from .tunes_lib import star_wars
from gpiozero.threads import GPIOThread
from threading import Event
from itertools import cycle

def tune_generator(tune):
    for note, duration in cycle(tune):
        yield (note, duration)

def play(event:Event, buzzer:TonalBuzzer, tune):
    for note, duration in tune_generator(tune):
        if not event.is_set():
            return
        buzzer.play(note)
        sleep(duration)

class Alarm(object):
    # GPIO Pin numbers, not the physical ones
    MAGNET_PIN = 17
    SWITCH_PIN = 20
    STATUS_LED_PIN = 27
    ALARM_ON_LED = 26
    BUZZER = 21

    def __init__(self):
        self.magnet = Button(self.MAGNET_PIN, bounce_time=0.05)
        self.status_led = LED(self.STATUS_LED_PIN)
        self.status_led.off()
        self.alarm_led = LED(self.ALARM_ON_LED)
        self.alarm_led.off()
        self.switch = Button(self.SWITCH_PIN, bounce_time=0.05)
        self.buzzer = TonalBuzzer(self.BUZZER, octaves=2)
        self.is_active = False
        self.is_raised = Event()
        self.is_raised.clear()

    def init_alarm(self):
        self.magnet.wait_for_active()
        self.status_led.on()  # Alarm will be triggered only when the door is closed
        self.is_active = True


    def raise_alarm(self):
        self.is_raised.set()
        self.alarm_led.on()
        # self.buzzer.source = sin_values()
        tone_thread = GPIOThread(target=play, args=(self.is_raised, self.buzzer, star_wars))
        tone_thread.start()
        # self.buzzer.source = self.play(star_wars)

    def stop(self):
        self.status_led.off()
        self.alarm_led.off()
        self.buzzer.source = None
        self.buzzer.stop()
        self.is_active = False
        self.is_raised.clear()

    def switch_state(self):
        if self.is_active:
            self.stop()
        else:
            self.init_alarm()

    def run(self):
        self.init_alarm()
        try:
            while True:
                if self.is_active and not self.is_raised.is_set() and not self.magnet.is_active:
                    self.raise_alarm()
                if self.switch.is_active:
                    self.switch_state()
                sleep(0.1)
        except KeyboardInterrupt:
            self.stop()
