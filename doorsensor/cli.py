# -*- coding: utf-8 -*-

"""Console script for doorsensor."""
import sys
import click
from .doorsensor import Alarm

@click.command()
def main(args=None):
    """Console script for doorsensor."""
    click.echo('starting...')
    alarm = Alarm()
    alarm.run()
    click.echo('end')
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
