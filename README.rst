============
Door Sensor
============


.. .. image:: https://img.shields.io/pypi/v/doorsensor.svg
        :target: https://pypi.python.org/pypi/doorsensor

.. .. image:: https://img.shields.io/travis/-/doorsensor.svg
        :target: https://travis-ci.org/-/doorsensor

.. .. image:: https://readthedocs.org/projects/doorsensor/badge/?version=latest
        :target: https://doorsensor.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Raspberry pi door sensor triggering an alarm


* Free software: GNU General Public License v3
.. * Documentation: https://doorsensor.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
